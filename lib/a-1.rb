# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  # is next num consecutive?
  #add each num from index to next
  in_between = []
  nums.each.with_index do |n, i|
    next_num = nums[i+1]
    if next_num == nil
      break
    elsif n+1 != next_num
      in_between << (n+1...next_num).to_a
    else
      next
    end
  end
  in_between.flatten
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  base_10 = 0
  binary.chars.reverse.each.with_index do |digit, i|
    if digit == '0'
      next
    else
      base_10 += (2**i)
    end
  end
  base_10
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    new_hash = {}

    self.each do |k, v|
      if prc.call k, v
        new_hash[k] = v
      end

    end

    new_hash
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
      #p self, hash
    new_h = {}
    if block_given? == true
      hash.each do |k, v|
        if self.key?(k)
          v2 = self.values_at(k)[0]
          x = prc.call k, v2, v
          new_h[k] = x
        else
          new_h[k] = v
        end
      end
    else
      hash.each {|k, v| self[k] = v}
      return self
    end
    new_h
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  l_num = 2
  l_num2 = 1
  move = 0
  if n > 0
    (1...n).to_a.each do |i|
      move = l_num + l_num2
      i.odd? == true ? l_num = move : l_num2 = move
    end
  elsif n < 0
    (1..n.abs).to_a.each do |i|
      if i.odd? == true
        move = l_num2 - l_num
        l_num2 = move
      else
        move = l_num - l_num2
        l_num = move
      end
    end
  else
    return 2
  end
  move
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  #set length to
  length = false

  string.chars.each_with_index do |let, i|
    chunk = string.slice(i, 3)

    if (string.reverse.include?(chunk)) && (chunk.length == 3)
      i1 = i
      i2 = string.reverse.index(chunk)
      l = length_of_palindrome(string, i1, i2)
      if length == false
        length = l
      elsif l > length
        length = l
      end
    end

  end
  length
end

def length_of_palindrome(string, i1, i2)
  count = 0
  nexti = i1

  while string[nexti] == string.reverse[i2]
    nexti += 1
    i2 += 1
    count += 1
  end

  count
end
